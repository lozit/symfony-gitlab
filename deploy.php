<?php

namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'git@gitlab.com:lozit/symfony-gitlab.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Shared files/dirs between deploys 
set('shared_files', []);
set('shared_dirs', ['var/log']);

// Writable dirs by web server 
set('writable_dirs', []);


// Hosts

host('178.62.50.186')
    ->user('root')
    ->port(22)
    ->set('deploy_path', '/var/www/deployer');


// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    //'deploy:vendors',
    'sf:vendors',
    'sf:clear_cache',
    //'sf:migrate',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

task('sf:vendors', function () {
    run(('cd {{release_path}} && composer install'));
});
task('sf:clear_cache', function () {
    run(('php {{release_path}}/bin/console cache:clear --env=prod'));
});
task('sf:migrate', function () {
    run(('php {{release_path}}/bin/console doctrine:migrations:migrate --env=prod'));
});
